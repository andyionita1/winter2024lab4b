import java.util.Scanner;

public class VirtualPetApp{
	
	public static void main(String[] args){
		
		Scanner reader = new Scanner(System.in);
		Parrot[] flock = new Parrot[2];
		
		for(int i = 0; i < flock.length; i++){
			System.out.println("What is the name of your Parrot");
			String Name = reader.nextLine();
			System.out.println("What is the Height of your Parrot (in cm)");
			int Height = Integer.parseInt(reader.nextLine());
			System.out.println("Is your Parrot an adult (True or false)");
			boolean Adult = Boolean.parseBoolean(reader.nextLine());
				
			flock[i] = new Parrot(Name, Adult, Height);
		}
		
		System.out.println("Change the name for the last parrot");
		flock[flock.length - 1].setName(reader.nextLine());
		
		System.out.println(flock[flock.length-1].getName());
		System.out.println(flock[flock.length-1].getHeight());
		System.out.println(flock[flock.length-1].getAdult());
	}
}