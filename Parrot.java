public class Parrot{
	
	private String Name;
	private boolean Adult;
	private int Height;
	
	public Parrot(String Name, boolean Adult, int Height) {
		this.Name = Name;
		this.Adult = Adult;
		this.Height = Height;
	}
	
	//Setter for Name
	public void setName(String Name) {
		this.Name = Name;
	}
	
	//Getter for Name
	public String getName() {
		return this.Name;
	}
	
	//Getter for Adult
	public boolean getAdult() {
		return this.Adult;
	}
	
	//Getter for Height
	public int getHeight() {
		return this.Height;
	}
	
	
	public void Fly(){
		
		if(Height < 0){
			System.out.println("Dead bird womp womp");
		}
		if(Adult && Height > 35){
			System.out.println("You fly very high good job top of the food chain");
		} else{
			System.out.println("You got eaten because of your smallness");
		}
	}
	
	public void Height(){
		System.out.println("Here's the length of your bird");
		for(int i = 0; i < Height; i++){
			System.out.println("|");
		}
	}
		
}
